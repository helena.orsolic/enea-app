# Aplikacija za Upravljanje Korisnicima Telekomunikacijske Tvrtke

Ova aplikacija je razvijena kao dio prakse u tvrtki Enea. Sastoji se od backend i frontend dijela koji omogućuju pregled i uređivanje popisa korisnika neke telekomunikacijske tvrtke.

## Backend

Backend sustav omogućuje sigurno povezivanje na funkcionalnu bazu podataka. Ovdje su smješteni podaci o korisnicima i pruža API za komunikaciju s frontend dijelom aplikacije.

## Frontend

Frontend dijelom aplikacije omogućeno je pregledavanje, uređivanje, brisanje i dodavanje novih korisnika. Također, korisnik može ispisati kompletni popis korisnika.


## Dodatne Informacije

Ovaj projekt je razvijen tijekom prakse u tvrtki Enea i služi kao demonstracija vještina u razvoju backend i frontend dijela aplikacija za upravljanje korisnicima. 

