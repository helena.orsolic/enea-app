let portNumber = 8081;
$(document).ready(function () {
  $("#subscribersTable").DataTable();
});


$(document).ready(function () {
  $("#search").on("click", function () {
    let phoneNumb = $("#phoneNumber").val();
    if (!(phoneNumb == "")) {
      var id = $("#phoneNumber").val();
      var url = "http://localhost:"+ portNumber +"/subscribers/" + id;
      $.ajax({
        type: "GET",
        url: url,
        success: function (response) {
          var table = $("#subscribersTable").DataTable();
          table.clear().destroy();
          var tr = "<tr>";
          tr += "<td>" + response.subscriber.id + "</td>";
          tr += "<td>" + response.subscriber.firstName + "</td>";
          tr += "<td>" + response.subscriber.lastName + "</td>";
          tr += "<td>" + response.subscriber.packageType + "</td>";
          tr += "<td>" + response.subscriber.phoneNumber + "</td>";
          tr += "<td>" + response.subscriber.email + "</td>";
          tr += "<td>" + response.subscriber.address + "</td>";
          tr += "<td>" + response.subscriber.oib + "</td>";
          tr +=
            "<td>" +
            "<div class='edit-delete-container'>" +
            "<button class='edit-button btn-config' id='edit" +
            response.subscriber.id +
            "'>Edit</button>" +
            "<button class='delete-button btn-config' id='delete" +
            response.subscriber.id +
            "'>Delete</button>" +
            "</div>" +
            "</td>" +
            "</tr>";
          $("#tBody").append(tr);
          $("#subscribersTable").DataTable();
        },
        error: function (xhr) {
          $("#responseMessage").css("color", "red");
          if (xhr.responseJSON != undefined) {
            $("#responseMessage").text(xhr.responseJSON.errors.subscriber);
          } else {
            $("#responseMessage").text("invalid request");
          }
        },
      });
    }
  });
});

$(document).ready(function () {
  $("#search").on("click", function () {
    var phoneNumb = $("#phoneNumber").val();
    if (phoneNumb == "") {
      var table = $("#subscribersTable").DataTable();
      table.clear().destroy();
      var url = "http://localhost:"+ portNumber +"/subscribers";
      $.ajax({
        type: "GET",
        url: url,
        success: function (response) {
          $.each(response, function (index, value) {
            var tr = "<tr>";
            tr += "<td>" + value.id + "</td>";
            tr += "<td>" + value.firstName + "</td>";
            tr += "<td>" + value.lastName + "</td>";
            tr += "<td>" + value.packageType + "</td>";
            tr += "<td>" + value.phoneNumber + "</td>";
            tr += "<td>" + value.email + "</td>";
            tr += "<td>" + value.address + "</td>";
            tr += "<td>" + value.oib + "</td>";
            tr +=
              "<td>" +
              "<div class='edit-delete-container'>" +
              "<button class='edit-button btn-config' type='submit' id='" +
              value.id +
              "'>Edit</button>" +
              "<button class='delete-button btn-config' type='submit' id='" +
              value.id +
              "'>Delete</button>" +
              "</div>" +
              "</td>" +
              "</tr>";
            $("#tBody").append(tr);
          });

          $("#subscribersTable").DataTable();
        },
      });
    }
  });
});

$("#subscribersTable").on("click", ".delete-button", function () {
  var table = $("#subscribersTable").DataTable();
  table.row($(this).closest("tr")).remove().draw();
  let id = $(this).attr("id");
  let url = "http://localhost:"+ portNumber +"/subscribers/";
  url += id;
  $.ajax({
    url: url,
    type: "DELETE",
    success: function (response) {
      $("#responseMessage").css("color", "green");
      $("#responseMessage").text(response.message);
    },
    error: function (xhr) {
      $("#responseMessage").css("color", "red");
      if (xhr.responseJSON != undefined) {
        $("#responseMessage").text(xhr.errors.subscriber);
      } else {
        $("#responseMessage").text("invalid request");
      }
    },
  });
});

$("#subscribersTable").on("click", ".edit-button", function () {
  var table = $("#subscribersTable").DataTable();
  var data = table.row($(this).closest("tr")).data();
  $("#addUserButton").val("upload");
  $("#submitText").text("Upload");
  $("#subscriberID").val(data[0]);
  $("#firstName").val(data[1]);
  $("#lastName").val(data[2]);
  $("#email").val(data[5]);
  $("#packageType").val(data[3]);
  $("#phoneNumberid").val(data[4]);
  $("#address").val(data[6]);
  $("#Oib").val(data[7]);
  if ($(".grid-container").hasClass("active")) {
    $(".grid-container").removeClass("active");
    $(".user-container").addClass("active");
  }
});

$(document).ready(function () {
  $("#addButton").on("click", function () {
    if ($(".grid-container").hasClass("active")) {
      $(".grid-container").removeClass("active");
      $(".user-container").addClass("active");
    }
  });
});
$(document).ready(function () {
  $("#backButton").on("click", function () {
    if ($(".user-container").hasClass("active")) {
      $(".user-container").removeClass("active");
      $(".grid-container").addClass("active");
    }
    $("#search").trigger("click");
    $("#addSubscriberMessage").text("");
    $("#addUserButton").val("add");
    $("#submitText").text("Add Subscriber");
    $("#subscriberID").val("");
    $("#firstName").val("");
    $("#lastName").val("");
    $("#email").val("");
    $("#packageType").val("");
    $("#phoneNumberid").val("");
    $("#address").val("");
    $("#Oib").val("");

    $("#firstName").css("background", "white");
    $("#firstName").attr("placeholder", "Your name..");

    $("#lastName").css("background", "white");
    $("#lastName").attr("placeholder", "Your last name..");

    $("#email").css("background", "white");
    $("#email").attr("placeholder", "Your email..");

    $("#packageType").css("background", "white");
    $("#packageType").attr("placeholder", "Prepaid or postpaid");

    $("#phoneNumberid").css("background", "white");
    $("#phoneNumberid").attr("placeholder", "Your phone number..");

    $("#address").css("background", "white");
    $("#address").attr("placeholder", "Your address..");

    $("#Oib").css("background", "white");
    $("#Oib").attr("placeholder", "Your OIB..");
  });
});
$(document).ready(function () {
  $("#addUserButton").click(function () {
    let url = "";
    let type = "";
    /* var phoneNumber = "+385"+ $("#phoneNumberid").val(); */ 
    if ($(this).val() == "add") {
      url = "http://localhost:"+ portNumber +"/subscribers";
      type = "POST";
    } else {
      url = "http://localhost:"+ portNumber +"/subscribers/";
      url += $("#subscriberID").val();
      type = "PUT";
    }
    let subscriber = {
      firstName: $("#firstName").val(),
      lastName: $("#lastName").val(),
      email: $("#email").val(),
      address: $("#address").val(),
      oib: $("#Oib").val(),
      phoneNumber: $("#phoneNumberid").val(),
      packageType: $("#packageType").val(),
    };
    $.ajax({
      url: url,
      type: type,
      data: JSON.stringify(subscriber),
      dataType: "json",
      contentType: "application/json",
      success: function (response) {
        $("#addUserButton").val("add");
        $("#submitText").text("Add Subscriber");
        $("#subscriberID").val("");
        $("#addSubscriberMessage").css("color", "green");
        $("#addSubscriberMessage").text(response.message);
        $("#firstName").val("");
        $("#lastName").val("");
        $("#email").val("");
        $("#packageType").val("");
        $("#phoneNumberid").val("");
        $("#address").val("");
        $("#Oib").val("");
      },
      error: function (xhr) {
        $("#addSubscriberMessage").css("color", "red");
        if (xhr.responseJSON != undefined) {
          $("#addSubscriberMessage").text(xhr.responseJSON.message);
          if (xhr.responseJSON.errors.firstName != undefined) {
            $("#firstName").css("background", "#ff4a4a7c");
            $("#firstName").val("");
            $("#firstName").attr(
              "placeholder",
              xhr.responseJSON.errors.firstName
            );
          }
          if (xhr.responseJSON.errors.lastName != undefined) {
            $("#lastName").css("background", "#ff4a4a7c");
            $("#lastName").val("");
            $("#lastName").attr(
              "placeholder",
              xhr.responseJSON.errors.lastName
            );
          }
          if (xhr.responseJSON.errors.email != undefined) {
            $("#email").css("background", "#ff4a4a7c");
            $("#email").val("");
            $("#email").attr("placeholder", xhr.responseJSON.errors.email);
          }
          if (xhr.responseJSON.errors.packageType != undefined) {
            $("#packageType").css("background", "#ff4a4a7c");
            $("#packageType").val("");
            $("#packageType").attr(
              "placeholder",
              xhr.responseJSON.errors.packageType
            );
          }
          if (xhr.responseJSON.errors.phoneNumber != undefined) {
            $("#phoneNumberid").css("background", "#ff4a4a7c");
            $("#phoneNumberid").val("");
            $("#phoneNumberid").attr(
              "placeholder",
              xhr.responseJSON.errors.phoneNumber
            );
          }
          if (xhr.responseJSON.errors.address != undefined) {
            $("#address").css("background", "#ff4a4a7c");
            $("#address").val("");
            $("#address").attr("placeholder", xhr.responseJSON.errors.address);
          }
          if (xhr.responseJSON.errors.oib != undefined) {
            $("#Oib").css("background", "#ff4a4a7c");
            $("#Oib").val("");
            $("#Oib").attr("placeholder", xhr.responseJSON.errors.oib);
          }
        } else {
          $("#addSubscriberMessage").text("invalid request");
        }
      },
    });
  });
});

$("#firstName").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your name..");
});
$("#lastName").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your last name..");
});
$("#email").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your email..");
});
$("#packageType").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Prepaid or postpaid");
});
$("#phoneNumberid").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your phone number..");
});
$("#address").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your address..");
});
$("#Oib").on("keyup", function () {
  $(this).css("background", "white");
  $(this).attr("placeholder", "Your OIB..");
});
$(document).ready(function() {
  
  $('#exportButton').on('click', function() {
    var table = $('#subscribersTable').DataTable();
    table.column(8).visible(false);
    $('#subscribersTable').tableExport({type:'pdf',
    jspdf: {orientation: 'p',
            margins: {left:20, top:10},
            autotable: false}
   }
   );
   setTimeout(
    function() 
    {
      table.column(8).visible(true);
    }, 20);
   
  });
  $('#exportButtonXLS').on('click', function() {
    var table = $('#subscribersTable').DataTable();
    table.column(8).visible(false);
    $('#subscribersTable').tableExport({type:'excel'});
    setTimeout(
      function() 
      {
        table.column(8).visible(true);
      }, 20);
  });
});
