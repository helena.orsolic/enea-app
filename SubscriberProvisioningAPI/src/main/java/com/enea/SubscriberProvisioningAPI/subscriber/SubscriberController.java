package com.enea.SubscriberProvisioningAPI.subscriber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
class SubscriberController {

    private final SubscriberRepository repository;

    private final SubscriberResponseBuilder response;

    SubscriberController(SubscriberRepository repository,SubscriberResponseBuilder response ) {
        this.repository = repository;

        this.response = response;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/subscribers")
    List<Subscriber> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @PostMapping("/subscribers")
    ResponseEntity<?> newSubscriber(@RequestBody Subscriber newSubscriber) {
        HashMap<String, Object> newResponse;
        newResponse = (HashMap<String, Object>) response.postResponse(newSubscriber);
        if( newResponse.get("status")== "error" ){
            return ResponseEntity.badRequest().body(newResponse);
        }
        return ResponseEntity.ok(newResponse);
    }

    @GetMapping("/subscribers/{phoneNumber}")
    ResponseEntity<?> getSubscriberPhone(@PathVariable String phoneNumber){
        HashMap<String, Object> newResponse;
        ArrayList<HashMap<String, Object>> list = new ArrayList<>();

        newResponse = (HashMap<String, Object>) response.getResponse(phoneNumber);
        list.add(newResponse);
        if( newResponse.get("status")== "error" ){
            return ResponseEntity.badRequest().body(newResponse);
        }
        return ResponseEntity.ok(newResponse);
    }
    @PutMapping("/subscribers/{id}")
    ResponseEntity<?> replaceSubscriber(@RequestBody Subscriber newSubscriber, @PathVariable Long id) {
        HashMap<String, Object> newResponse;
        newResponse = (HashMap<String, Object>) response.putResponse(newSubscriber,id);
        if( newResponse.get("status")== "error" ){
            return ResponseEntity.badRequest().body(newResponse);
        }
        return ResponseEntity.ok(newResponse);
    }

    @DeleteMapping("/subscribers/{id}")
    ResponseEntity<?> deleteSubscriber(@PathVariable Long id) {
        HashMap<String, Object> newResponse;
        newResponse = (HashMap<String, Object>) response.deleteResponse(id);
        if( newResponse.get("status")== "error" ){
            return ResponseEntity.badRequest().body(newResponse);
        }
        return ResponseEntity.ok(newResponse);
    }
}