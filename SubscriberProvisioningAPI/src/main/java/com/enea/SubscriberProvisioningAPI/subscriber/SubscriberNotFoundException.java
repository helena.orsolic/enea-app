package com.enea.SubscriberProvisioningAPI.subscriber;

public class SubscriberNotFoundException extends RuntimeException {

    SubscriberNotFoundException(Long id) {
        super("Could not find subscriber " + id);
    }
}
