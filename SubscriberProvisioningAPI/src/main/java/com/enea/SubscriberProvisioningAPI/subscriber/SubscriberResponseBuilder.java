package com.enea.SubscriberProvisioningAPI.subscriber;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.lang.Object;


@Component
public class SubscriberResponseBuilder {


    private final SubscriberValidation validation;

    SubscriberResponseBuilder( SubscriberValidation validation){
        this.validation = validation;
    }
    public Map<String,Object> postResponse(Object newSubscriber){
        String message;
        Map<String,Object> errors = new HashMap<>();
        Map<String,Object> response = new HashMap<>();
        message = validation.validateId(newSubscriber);
        if(!message.equals("")){
            errors.put("id",message);
        }
        message = validation.validateFirstName(newSubscriber);
        if(!message.equals("")){
            errors.put("firstName",message);
        }
        message = validation.validateLastName(newSubscriber);
        if(!message.equals("")){
            errors.put("lastName",message);
        }
        message = validation.validatePhoneNumber(newSubscriber);
        if(!message.equals("")){
            errors.put("phoneNumber",message);
        }
        message = validation.validateOib(newSubscriber);
        if(!message.equals("")){
            errors.put("oib",message);
        }
        message = validation.validateAddress(newSubscriber);
        if(!message.equals("")){
            errors.put("address",message);
        }
        message = validation.validateEmail(newSubscriber);
        if(!message.equals("")){
            errors.put("email",message);
        }
        message = validation.validatePackageType(newSubscriber);
        if(!message.equals("")){
            errors.put("packageType",message);
        }
        if(errors.isEmpty()){
            response.put("status","success");
            response.put("message","Subscriber is added");
            validation.saveSubscriber(newSubscriber);
            return response;
        }
        response.put("status","error");
        response.put("message","Subscriber is not added");
        response.put("errors",errors);
        return response;
    }
    public Map<String,Object> putResponse(Object newSubscriber,Long id){
        String message;
        Map<String,Object> errors = new HashMap<>();
        Map<String,Object> response = new HashMap<>();
        if(!validation.isSubscriberPresentById(id)){
            errors.put("subscriber","Subscriber does not exist");
            response.put("status","error");
            response.put("message","Subscriber is not updated");
            response.put("errors",errors);
            return response;
        }

        Subscriber existingSubscriber = validation.getSubscriberById(id);
        Subscriber subscriber = (Subscriber) newSubscriber;

        if((subscriber.getFirstName() != null) && !subscriber.getFirstName().isEmpty()){
            if(!existingSubscriber.getFirstName().equals(subscriber.getFirstName())) {
                message = validation.validateFirstName(newSubscriber);
                if (!message.equals("")) {
                    errors.put("firstName", message);
                } else {
                    existingSubscriber.setFirstName(subscriber.getFirstName());
                }
            }
        }
        if(subscriber.getLastName() != null && !subscriber.getLastName().isEmpty()){
            if(!existingSubscriber.getLastName().equals(subscriber.getLastName())) {
                message = validation.validateLastName(newSubscriber);
                if (!message.equals("")) {
                    errors.put("lastName", message);
                } else {
                    existingSubscriber.setLastName(subscriber.getLastName());
                }
            }
        }
        if(subscriber.getPhoneNumber() != null && !subscriber.getPhoneNumber().isEmpty()){
            if(!existingSubscriber.getPhoneNumber().equals(subscriber.getPhoneNumber())) {
                message = validation.validatePhoneNumber(newSubscriber);
                if (!message.equals("")) {
                    errors.put("phoneNumber", message);
                } else {
                    existingSubscriber.setPhoneNumber(subscriber.getPhoneNumber());
                }
            }
        }
        if(subscriber.getEmail() != null && !subscriber.getEmail().isEmpty()){
            if(!existingSubscriber.getEmail().equals(subscriber.getEmail())) {
                message = validation.validateEmail(newSubscriber);
                if (!message.equals("")) {
                    errors.put("email", message);
                } else {
                    existingSubscriber.setEmail(subscriber.getEmail());
                }
            }
        }

        if(subscriber.getPackageType() != null && !subscriber.getPackageType().isEmpty()){
            if(!existingSubscriber.getPackageType().equals(subscriber.getPackageType())) {
                message = validation.validatePackageType(newSubscriber);
                if (!message.equals("")) {
                    errors.put("packageType", message);
                } else {
                    existingSubscriber.setPackageType(subscriber.getPackageType());
                }
            }
        }
        if(subscriber.getAddress() != null && !subscriber.getAddress().isEmpty()){
            if(!existingSubscriber.getAddress().equals(subscriber.getAddress())) {
                message = validation.validateAddress(newSubscriber);
                if (!message.equals("")) {
                    errors.put("address", message);
                } else {
                    existingSubscriber.setAddress(subscriber.getAddress());
                }
            }
        }
        if(subscriber.getOIB() != null && !subscriber.getOIB().isEmpty()){
            if(!existingSubscriber.getOIB().equals(subscriber.getOIB())) {
                message = validation.validateOib(newSubscriber);
                if (!message.equals("")) {
                    errors.put("oib", message);
                } else {
                    existingSubscriber.setOIB(subscriber.getOIB());
                }
            }
        }
        if(errors.isEmpty()){
            response.put("status","success");
            response.put("message","Subscriber is updated");
            validation.saveSubscriber(existingSubscriber);
            return response;
        }
        response.put("status","error");
        response.put("message","Subscriber is not updated");
        response.put("errors",errors);
        return response;
    }
    public Map<String,Object> deleteResponse(Long id){
        Map<String,Object> response = new HashMap<>();
        if(validation.isSubscriberPresentById(id)){
            response.put("status","success");
            response.put("message","Subscriber is deleted");
            validation.deleteSubscriber(id);
            return response;
        }
        Map<String,Object> errors = new HashMap<>();
        errors.put("subscriber","Subscriber does not exist");
        response.put("status","error");
        response.put("message","Subscriber is not deleted");
        response.put("errors",errors);
        return response;
    }
    public Map<String,Object> getResponse(String phoneNumber){
        Map<String,Object> response = new HashMap<>();
        if(validation.isSubscriberPresentByPhoneNumber(phoneNumber)){
            response.put("status","success");
            response.put("message","Subscriber is found");
            response.put("subscriber",validation.getSubscriberByPhoneNumber(phoneNumber));
            return response;
        }
        Map<String,Object> errors = new HashMap<>();
        String message;
        message = "subscriber with number " + phoneNumber + " does not exist";
        errors.put("subscriber",message);
        response.put("status","error");
        response.put("message","Subscriber is not found");
        response.put("errors",errors);
        return response;
    }
}
