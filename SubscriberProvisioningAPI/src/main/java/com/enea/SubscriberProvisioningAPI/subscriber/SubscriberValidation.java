package com.enea.SubscriberProvisioningAPI.subscriber;

import org.springframework.stereotype.Component;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class SubscriberValidation {
    private final SubscriberRepository repository;
    SubscriberValidation(SubscriberRepository repository) {
        this.repository = repository;
    }

    public String validateId(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getId() != null) {
            Optional<Subscriber> existingSubscriber = repository.findById(subscriber.getId());
            if (existingSubscriber.isPresent()) {
                return "already exists";
            }
        }
        return "";
    }

    public String validateFirstName(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getFirstName() == null || subscriber.getFirstName().isEmpty() || (subscriber.getFirstName().length() <= 1)) {
            return "First Name must contain more than one letter";
        }
        return "";
    }

    public String validateLastName(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getLastName() == null || subscriber.getLastName().isEmpty() || (subscriber.getLastName().length() <= 1)) {
            return "Last Name must contain more than one letter";
        }
        return "";
    }

    public String validatePhoneNumber(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getPhoneNumber() == null || subscriber.getPhoneNumber().isEmpty()) {
            return "Phone number is required";
        }
        System.out.println(subscriber.getPhoneNumber());
        String regex = "^\\+3859\\d{7,8}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(subscriber.getPhoneNumber());
        if (!matcher.matches()) {
            return "incorrect number";
        }
        if (repository.findSubscriberByPhoneNumber(subscriber.getPhoneNumber()).isPresent()) {
            return "already exists";
        }
        return "";
    }

    public String validateEmail(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getEmail() == null || subscriber.getEmail().isEmpty()) {
            return "Email is required";
        }
        String regex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(subscriber.getEmail());
        if (!matcher.matches()) {
            return "incorrect email";
        }
        return "";
    }

    public String validateOib(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getOIB() == null || subscriber.getOIB().isEmpty()) {
            return "OIB is required";
        }
        if (repository.findSubscriberByOIB(subscriber.getOIB()) != null) {
            return "already exists";
        }
        if (subscriber.getOIB().length() != 11) {
            return "must contain 11 numbers";
        }
        String regex = "[0-9]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(subscriber.getOIB());
        if (!matcher.matches()) {
            return "must contain only numbers";
        }

        return "";
    }

    public String validatePackageType(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getPackageType() == null || subscriber.getPackageType().isEmpty()) {
            return "Package type is required";
        }
        if(!(subscriber.getPackageType().equalsIgnoreCase("prepaid") || subscriber.getPackageType().equalsIgnoreCase("postpaid"))){
            return "Package type must be prepaid or postpaid";
        }
        return "";
    }

    public String validateAddress(Object newSubscriber) {
        Subscriber subscriber = (Subscriber) newSubscriber;
        if (subscriber.getAddress() == null || subscriber.getAddress().isEmpty()) {
            return "Address is required";
        }
        return "";
    }

    public void saveSubscriber(Object newSubscriber) {
        repository.save((Subscriber) newSubscriber);
    }
    public void deleteSubscriber(Long id){
        repository.deleteById(id);
    }
    public boolean isSubscriberPresentById(Long id){
        return repository.findById(id).isPresent();
    }

    public Subscriber getSubscriberById(Long id){
        return repository.findById(id).get();
    }
    public boolean isSubscriberPresentByPhoneNumber(String phoneNumber){
        return repository.findSubscriberByPhoneNumber(phoneNumber).isPresent();
    }
    public Subscriber getSubscriberByPhoneNumber(String phoneNumber){
        return repository.findSubscriberByPhoneNumber(phoneNumber).get();
    }
}
