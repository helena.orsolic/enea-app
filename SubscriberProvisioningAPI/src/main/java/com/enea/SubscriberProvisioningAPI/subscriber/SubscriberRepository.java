package com.enea.SubscriberProvisioningAPI.subscriber;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    Optional <Subscriber> findSubscriberByPhoneNumber(String phoneNumber);
    Subscriber findSubscriberByOIB(String OIB);
}