package com.enea.SubscriberProvisioningAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class SubscriberProvisioningApiApplication {
 
	public static void main(String[] args) {
		SpringApplication.run(SubscriberProvisioningApiApplication.class, args);
	}


}
